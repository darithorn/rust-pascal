mod lexer;
mod error;
mod parser;
mod ast;

use lexer::*;
use parser::*;
// use error::*;

fn main() {
    let mut lexer = Lexer::new("test.pas");
    let mut tok = lexer.next_token();
    loop {
        println!("{:?}", tok);
        if let Token::None = tok {
            break;
        }
        tok = lexer.next_token();
    }
    for err in lexer.errors.iter() {
        println!("{}", err);
    }

    let parser = Parser::new(&mut lexer);
}
