pub enum Ast {
    Identifier(String, Option<Box<Ast>>),
    Parameter {
        name: String,
        type_ident: Box<Ast>, 
    },
    Procedure {
        name: String,
        parameters: Vec<Box<Ast>>,
        var: Option<Box<Ast>>,
        block: Option<Box<Ast>>,
    },
    Var(Vec<Ast>),
    Block(Vec<Ast>),
}
