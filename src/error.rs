use std::fmt;
use std::fmt::{Formatter, Debug, Display};

pub struct Error {
    msg: String,
    line: usize,
    col: usize
}

impl Error {
    pub fn new(line: usize, col: usize, msg: String) -> Error {
        Error {
            msg: msg,
            line: line,
            col: col
        }
    }
}

impl Debug for Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "[{}:{}] {}", self.line, self.col, self.msg)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "[{}:{}] {}", self.line, self.col, self.msg)
    }
}
