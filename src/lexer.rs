use std::fs::File;
use std::io::Read;
use std::path::Path;
use error::*;

#[derive(Debug, PartialEq)]
pub enum Token {
    None,
    Integer(i32),
    Str(String),
    Ident(String),
    IntegerType,
    Procedure,
    Var,
    Type,
    Begin,
    End,
    Return,

    Operator(char),
    SemiColon,
    Colon,
    Assign,
    Comma,
    Dot,
    OpenParen,
    CloseParen,
    OpenBracket,
    CloseBracket,
}

pub struct Lexer {
    pub errors: Vec<Error>,
    length: usize,
    contents: Vec<char>,
    position: usize,
    pub line: usize,
    pub col: usize,
    // Used for errors
    tmp_line :usize,
    tmp_col: usize,
}

impl Lexer {
    pub fn new<P: AsRef<Path>>(path: P) -> Lexer {
        let mut file = File::open(path).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        let chars: Vec<char> = contents.chars().collect();
        Lexer {
            errors: Vec::new(),
            length: chars.len(),
            contents: chars,
            position: 0,
            line: 1,
            col: 1,
            tmp_line: 0,
            tmp_col: 0,
        }
    }

    fn set_tmp(&mut self) {
        self.tmp_line = self.line;
        self.tmp_col = self.col;
    }

    fn error(&mut self, msg: String) {
        let err = Error::new(self.tmp_line, self.tmp_col, msg);
        self.errors.push(err);
    }

    fn get_ident<F>(&mut self, mut f: F) -> String
            where F: FnMut(&char) -> bool {
        let ident: String = self.contents[self.position - 1..]
                                .iter()
                                .take_while(|c| f(c))
                                .collect();
        // - 1 because we've already read the first character
        self.position += ident.len() - 1;
        ident
    }

    fn next_char(&mut self) -> Option<char> {
        if self.position >= self.length {
            None
        } else {
            let tmp = self.contents[self.position];
            if tmp == '\n' {
                self.line += 1;
                self.col = 1;
            } else {
                self.col += 1;
            }
            self.position += 1;
            Some(tmp)
        }
    }

    fn peek_char(&self) -> Option<char> {
        let pos = self.position;
        if pos >= self.length {
            None
        } else {
            Some(self.contents[pos])
        }
    }

    pub fn peek_token(&mut self) -> Token {
        let pos = self.position;
        let line = self.line;
        let col = self.col;
        let tok = self.next_token();
        self.position = pos;
        self.line = line;
        self.col = col;
        return tok;
    }

    pub fn next_token(&mut self) -> Token {
        self.set_tmp();
        let mut c = match self.next_char() {
            Some(c) => c,
            None => { return Token::None; }
        };
        loop {
            if c.is_whitespace() {
                self.set_tmp();
                c = match self.next_char() {
                    Some(c) => c,
                    None => { return Token::None; }
                };
            } else {
                break;
            }
        }
        if c.is_alphabetic() {
            let word = self.get_ident(|c| c.is_alphabetic());
            match word.as_str() {
                "end" => Token::End,
                "begin" => Token::Begin,
                "return" => Token::Return,
                "procedure" => Token::Procedure,
                "var" => Token::Var,
                "type" => Token::Type,
                "integer" => Token::IntegerType,
                _ => Token::Ident(word)
            }
        } else if c.is_numeric() {
            self.set_tmp();
            let num = self.get_ident(|c| c.is_numeric());
            match num.parse::<i32>() {
                Ok(n) => Token::Integer(n),
                Err(_) => {
                    self.error(format!("{} is not a number", num));
                    Token::None
                }
            }
        } else {
            match c {
                '"' => {
                    let mut result = String::new();
                    loop {
                        let c = self.next_char();
                        if let Some(ch) = c {
                            if ch == '\\' {
                                let f = self.peek_char();
                                if let Some(fh) = f {
                                    match fh {
                                        '"' => result.push('"'),
                                        'n' => result.push('\n'),
                                        _ => {
                                            self.error(format!("\\{} is an unknown escape", fh));
                                        }
                                    }
                                } else {
                                    // TODO: error here?
                                }
                                self.next_char();
                            } else if ch == '"' {
                                break;
                            } else {
                                result.push(ch);
                            }
                        } else {
                            self.error(format!("Expected closing \""));
                            break;
                        }
                    }
                    Token::Str(result)
                },
                ';' => Token::SemiColon,
                ':' => {
                    if let Some(tmp) = self.peek_char() {
                        if tmp == '=' {
                            self.next_char();
                            Token::Assign
                        } else {
                            Token::Colon
                        }
                    } else {
                        Token::Colon
                    }
                },
                '+' | '-' | '/' | '*' => Token::Operator(c),
                ',' => Token::Comma,
                '.' => Token::Dot,
                '(' => Token::OpenParen, 
                ')' => Token::CloseParen,
                '[' => Token::OpenBracket,
                ']' => Token::CloseBracket,
                _ => {
                    self.error(format!("Unknown character {}", c));
                    Token::None
                }
            }
        }
    }
}
