use error::*;
use lexer::*;
use ast::*;

pub struct Parser<'a> {
    lexer: &'a mut Lexer,
    errors: Vec<Error>,
    line: usize,
    col: usize
}

impl<'a> Parser<'a> {
    pub fn new(lexer: &'a mut Lexer) -> Parser {
        let line = lexer.line;
        let col = lexer.col;
        Parser {
            lexer: lexer,
            errors: Vec::new(),
            line: line,
            col: col
        }
    }

    fn set_pos(&mut self) {
        self.line = self.lexer.line;
        self.col = self.lexer.col;
    }

    fn error(&mut self, msg: String) {
        let err = Error::new(self.line, self.col, msg);
        self.errors.push(err);
    }

    fn expect(&mut self, tok: Token) -> bool {
        self.next_token() == tok
    }

    fn next_token(&mut self) -> Token {
        self.set_pos();
        self.lexer.next_token()
    }

    pub fn parse_ident(&mut self) -> Option<Ast> {
        let ident = if let Token::Ident(s) = self.next_token() {
            s
        } else {
            self.error("Expected identifier".to_string());
            return None;
        };
        let next = self.lexer.peek_token();
        let rhs: Option<Box<Ast>> = if next == Token::Dot {
            self.next_token();
            match self.parse_ident() {
                Some(ast) => Some(Box::new(ast)),
                None => {
                    return None;
                }
            }
        } else {
            None
        };
        Some(Ast::Identifier(ident, rhs))
    }

    pub fn parse_var(&mut self) -> Option<Ast> {
        None
    }

    pub fn parse_block(&mut self) -> Option<Ast> {
        Some(Ast::Block(Vec::new()))
    }

    pub fn parse_procedure(&mut self) -> Option<Ast> {
        // assert!(self.lexer.cur_tok == Token::Procedure); 
        let name = if let Token::Ident(s) = self.next_token() {
            s
        } else {
            self.error("Expected identifier".to_string());
            return None;
        };

        let mut parameters: Vec<Box<Ast>> = Vec::new();
        if self.expect(Token::OpenParen) {
            loop {
                let param_name = if let Token::Ident(s) = self.next_token() {
                    s
                } else {
                    self.error("Expected parameter name".to_string());
                    return None;
                };
                if !self.expect(Token::Colon) {
                    self.error("Expected colon".to_string());
                    return None;
                }
                let type_name = match self.parse_ident() {
                    Some(ast) => ast,
                    None => {
                        return None;
                    }
                };
                parameters.push(Box::new(Ast::Parameter {
                    name: param_name,
                    type_ident: Box::new(type_name)
                }));
                let peek = self.lexer.peek_token();
                if let Token::CloseParen = peek {
                    self.next_token();
                    break;
                } else if let Token::None = peek {
                    self.error("Expected closing parenthesis".to_string());
                    return None;
                }
            }
        }

        let var = if let Token::Var = self.lexer.peek_token() {
            match self.parse_var() {
                Some(ast) => Some(Box::new(ast)),
                None => {
                    return None;
                }
            }
        } else {
            None
        };

        let block = if let Token::Begin = self.lexer.peek_token() {
            match self.parse_block() {
                Some(ast) => Some(Box::new(ast)),
                None => {
                    return None;
                }
            }
        } else {
            if var.is_some() {
                self.error("Expected a begin/end block after var".to_string());
                return None;
            }
            self.error("Expected begin/end block for implementation".to_string());
            return None;
        };

        // The semi-colon is optional
        if let Token::SemiColon = self.lexer.peek_token() {
            self.next_token();
        }

        Some(Ast::Procedure {
            name: name,
            parameters: parameters,
            block: block,
            var: var
        })
    }
}
